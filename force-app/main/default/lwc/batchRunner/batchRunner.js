/**
 * Created by alexanderbelenov on 11.06.2022.
 */

import { LightningElement } from "lwc";
import getBatches from '@salesforce/apex/BatchJobController.getBatches'
import columns from "./columns";
import abortBatch from '@salesforce/apex/BatchJobController.abortBatch'
import runBatch from '@salesforce/apex/BatchJobController.runBatch'
export default class BatchRunner extends LightningElement {
  columns = columns;
  modalBatchId;
  batches;
  connectedCallback() {
    getBatches().then(result => {
      this.batches = result;
      console.log(result);
    }).catch(error => {
      console.log(error);
    });
  }
  handleRowAction(event) {
    if (event.detail.action.name === 'showModal') {
      this.showModalWindow(event.detail.row.Id);
    } else if (event.detail.action.name ==='runBatch') {
      console.log(event.detail.row.Name);
      this.runNewBatch(event.detail.row.Name);
    } else if ('abortBatch' === event.detail.action.name) {
      this.abort(event.detail.row.Id);
    }
  }
  runNewBatch(batchName) {
    runBatch({ asyncJobName: batchName }).then(result => {
      console.log(result);
    }).catch(error => {
      console.log(error);
    });
  }
  abort(batchId) {
    abortBatch({ batchId: batchId }).then(result => {
      this.batches = this.batches.map(old => {
        console.log(old);
        console.log(result);
        console.log(old.Id === result.Id)
        console.log('-------------');
        return old.Id !== result.Id ? old : result;
      });
    }).catch(error => {
      console.log(error);
    });
  }
  showModalWindow(id) {
    this.modalBatchId = id;
  }
  closeModal() {
    this.modalBatchId = ''
  }
}