
const runAction = {
  label: 'Run',
  iconName: 'utility:connected_apps',
  name: 'runBatch'
};
const abortAction = {
  label: 'Abort',
  iconName: 'utility:error',
  name: 'abortBatch'
};

const modalAction = {
  label: 'Modal',
  iconName: 'action:preview',
  name: 'showModal'
}

let columns = [
  {
    fieldName: 'Name',
    label: 'Batch name',
    type: 'String'
  }, {
    fieldName: 'Status',
    label: 'Status',
    type: 'String'
  }, {
    fieldName: 'CompletedDate',
    label: 'Completed Date',
    type: 'date'
  }, {
    type: 'action',
    typeAttributes: {
      menuAlignment: 'right',
      rowActions: (row, doneCallback) => {
        const actions = [];
        if (row.Status === "Processing") {
          actions.push(abortAction);
        }
        if (row.Status !== "Processing") {
          actions.push(runAction);
        }
        if (row.Status === "Error" || (row.Status === "Completed" && row.NumberOfErrors > 0)) {
          actions.push(modalAction);
        }
        doneCallback(actions);
      }
    }
  }
];

export default columns;