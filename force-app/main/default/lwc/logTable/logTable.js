/**
 * Created by alexanderbelenov on 11.06.2022.
 */

import { LightningElement, api, wire, track } from "lwc";
import logsLabels from '@salesforce/apex/LogcController.getLabels'
import logsData from '@salesforce/apex/LogcController.getLogs'

export default class LogTable extends LightningElement {
  @track columns;
  @track logs;
  @api contextId;
  @wire(logsLabels)
  tableColumns({error, data}){
    if (data) {
      this.columns = data;
    } else if (error) {
      console.log(error);
    }
  }
  @wire(logsData, {batchId:'$contextId'})
  logData({error, data}){
    if (data) {
      this.logs = data;
    } else if (error) {
      console.log(error);
    }
  }
}