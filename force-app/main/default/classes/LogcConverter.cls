/**
 * Created by alexanderbelenov on 11.06.2022.
 */

public with sharing class LogcConverter {
    public static List<LogcColumn> sObjectFieldToLogcColumn(List<SObjectField> fieldList) {
        final List<LogcColumn> logcColumns = new List<LogcColumn>();
        for (SObjectField field : fieldList) {
            LogcColumn column = sObjectFieldToLogcColumn(field);
            logcColumns.add(column);
        }
        return logcColumns;
    }
    public static LogcColumn sObjectFieldToLogcColumn(SObjectField field) {
        final LogcColumn column = new LogcColumn();
        column.label = field.getDescribe().label;
        column.fieldName = field.getDescribe().name;
        column.type = field.getDescribe().type.name();
        return column;
    }

    public static List<String> sObjectFieldToFieldName(List<SObjectField> fieldList) {
        final List<String> fieldNameList = new List<String>();
        for (SObjectField field : fieldList) {
            String fieldName = sObjectFieldToFieldName(field);
            fieldNameList.add(fieldName);
        }
        return fieldNameList;
    }

    public static String sObjectFieldToFieldName(SObjectField field) {
        String fieldName = field.getDescribe().name;
        return fieldName;
    }

}