/**
 * Created by alexanderbelenov on 11.06.2022.
 */
public with sharing class LogcColumn {

    @AuraEnabled
    public Id id {set; get;}

    @AuraEnabled
    public String label {set; get;}

    @AuraEnabled
    public String fieldName {set; get;}

    @AuraEnabled
    public String type {set; get;}
}