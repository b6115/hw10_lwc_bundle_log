/**
 * Created by alexanderbelenov on 11.06.2022.
 */

public with sharing class LogcController {
    private static LogcService service = LogcService.getInstance();

    @AuraEnabled(cacheable=true)
    public static List<LogcColumn> getLabels() {
       return service.getLabels();
    }

    @AuraEnabled(cacheable=true)
    public static List<Log__c> getLogs(Id batchId) {
        List<Log__c> result = null;
        result = service.getLogs(batchId);
        return result;
    }
}