/**
 * Created by alexanderbelenov on 11.06.2022.
 */

public with sharing class LogcService {

    private LogcService() {}

    public List<LogcColumn> getLabels() {
        final List<LogcColumn> result = LogcConverter.sObjectFieldToLogcColumn(
                TablesConstants.DEFAULT_LOGC_TABLE_FIELDS
        );
        return result;
    }

    public List<Log__c> getLogs(Id batchId) {
        //TODO: Ask about this approach of query generation
        final List<String> columns = LogcConverter.sObjectFieldToFieldName(
                TablesConstants.DEFAULT_LOGC_TABLE_FIELDS
        );
        final String queryFields = String.join(columns, ', ');
        final String context = '\''+batchId+'\'';
        final String fQuery = String.format(TablesConstants.LOGC_IN_CONTEXT_QUERY, new String[] {queryFields, context});
        List<Log__c> result = Database.query(fQuery);
        return result;
    }

    private static LogcService instance;
    public static LogcService getInstance() {
        if (instance == null) {
            instance = new LogcService();
        }
        return instance;
    }
}