/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class AsyncJobConverter {

    public static List<AsyncJobData> asyncApexJobToAsyncJobData(List<AsyncApexJob> jobList) {
        List<AsyncJobData> result = new List<AsyncJobData>();
        for (AsyncApexJob job : jobList) {
            final AsyncJobData data = asyncApexJobToAsyncJobData(job);
            result.add(data);
        }
        return result;
    }

    public static AsyncJobData asyncApexJobToAsyncJobData(AsyncApexJob job) {
        final AsyncJobData result = new AsyncJobData();
        result.Id = job.Id;
        result.Name = job.ApexClass.Name;
        result.Status = job.Status;
        result.CompletedDate = job.CompletedDate;
        result.NumberOfErrors = job.NumberOfErrors;
        return result;
    }
}