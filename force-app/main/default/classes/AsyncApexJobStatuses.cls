/**
 * Created by alexanderbelenov on 12.06.2022.
 */

// TODO: Ask a question: How can I get a set of possible async jobs' statuses to avoid the hardcoding
public with sharing class AsyncApexJobStatuses {
    public static final String PROCESSING = 'Processing';
    public static final String ABORTED = 'Aborted';
    public static final String ERROR = 'Error';
    public static final String COMPLETED = 'Completed';
}