/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class AsyncJobNames {
    /*
     * key: String - it's an asynchronous job's name
     * value: IAsyncJobFactory - it's a factory, which created instances of the job name
     */
    private static final Map<String, IAsyncJobFactory> asyncFactories = new Map<String, IAsyncJobFactory>{
            'VacancyBatch' => new VacancyBatchFactory()
    };

    public static Boolean isRegistered(String jobName) {
        return asyncFactories.keySet().contains(jobName);
    }

    public static IAsyncJobFactory getFactory(String jobName) {
        IAsyncJobFactory factory = null;
        if (isRegistered(jobName)) {
            factory = asyncFactories.get(jobName);
        }
        return factory;
    }
}