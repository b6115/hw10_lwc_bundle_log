/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class BatchJobController {
    private static BatchJobService service = BatchJobService.getInstance();

    @AuraEnabled
    public static List<AsyncJobData> getBatches() {
        List<AsyncJobData> result = service.getBatches();
        return result;
    }

    @AuraEnabled
    public static AsyncJobData abortBatch(Id batchId) {
        final AsyncJobData result = service.abortBatch(batchId);
        return result;
    }

    @AuraEnabled
    public static Id runBatch (String asyncJobName) {
        return service.runBatch(asyncJobName);
    }
}