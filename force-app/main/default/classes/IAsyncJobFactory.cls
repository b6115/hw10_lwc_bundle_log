/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public interface IAsyncJobFactory {
    Database.Batchable<SObject> create();
}