/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class VacancyBatchFactory implements IAsyncJobFactory {
    public Database.Batchable<SObject> create() {
        return new VacancyBatch();
    }
}