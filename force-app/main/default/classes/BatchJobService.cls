/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class BatchJobService {
    private BatchJobService() {}

    public List<AsyncJobData> getBatches() {
        final List<AsyncApexJob> jobList = Database.query(TablesConstants.BATCH_ASYNCJOB_QUERY);
        final List<AsyncJobData> result = AsyncJobConverter.asyncApexJobToAsyncJobData(jobList);
        return result;
    }

    public Id runBatch (String asyncJobName) {
        if (AsyncJobNames.isRegistered(asyncJobName)) {
            Database.Batchable<SObject> batch = AsyncJobNames.getFactory(asyncJobName).create();
            Id id = Database.executeBatch(batch, 1);
            System.debug(id);
            return id;
        } else {
            return '';
        }
    }

    public AsyncJobData abortBatch(Id batchId) {
        AsyncApexJob job = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :batchId];
        if (job.Status == AsyncApexJobStatuses.PROCESSING) {
            System.abortJob(job.id);
        }
        job = [
                SELECT Id, ApexClass.Name, Status, CompletedDate, NumberOfErrors
                FROM AsyncApexJob
                WHERE Id = :batchId
        ];
        final AsyncJobData result = AsyncJobConverter.asyncApexJobToAsyncJobData(job);
        return result;
    }

    private static BatchJobService instance;
    public static BatchJobService getInstance() {
        if (instance == null) {
            instance = new BatchJobService();
        }
        return instance;
    }
}

