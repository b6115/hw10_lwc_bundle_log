/**
 * Created by alexanderbelenov on 11.06.2022.
 */

public with sharing class TablesConstants {
    public static final List<SObjectField> DEFAULT_LOGC_TABLE_FIELDS = new List<SObjectField> {
            Log__c.Name,
            Log__c.Message__c,
            Log__c.Stack_Trace_String__c
    };
    public static final String LOGC_IN_CONTEXT_QUERY = 'SELECT {0} FROM Log__c WHERE Context_Id__c In ({1})';
    public static final String BATCH_ASYNCJOB_QUERY = 'SELECT Id, ApexClass.Name, Status, CompletedDate, NumberOfErrors FROM AsyncApexJob WHERE JobType = \'BatchApex\'';
}