/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class AsyncJobData {
    @AuraEnabled
    public Id Id {set; get;}

    @AuraEnabled
    public String Name {set; get;}

    @AuraEnabled
    public String Status {set; get;}

    @AuraEnabled
    public Datetime CompletedDate {set; get;}

    @AuraEnabled
    public Integer NumberOfErrors {set; get;}
}