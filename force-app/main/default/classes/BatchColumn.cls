/**
 * Created by alexanderbelenov on 12.06.2022.
 */

public with sharing class BatchColumn {

    @AuraEnabled
    public String label {set; get;}

    @AuraEnabled
    public String fieldName {set; get;}

    @AuraEnabled
    public String type {set; get;}
}